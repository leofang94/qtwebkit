Name:           qtwebkit
Version:        2.3.4
Release:        29
Summary:        Qt WebKit bindings
License:        LGPLv2 with exceptions or GPLv3 with exceptions
URL:            http://trac.webkit.org/wiki/QtWebKit
Source0:        https://download.kde.org/stable/qtwebkit-2.3/%{version}/src/qtwebkit-%{version}.tar.gz
Source1:        qmake.sh
Patch0001:      qtwebkit-build_with_g1.patch
Patch0002:      qtwebkit-reduce_memory_overheads.patch
BuildRequires:  bison gperf flex gcc-c++ libicu-devel libjpeg-devel pkgconfig(gio-2.0) pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(fontconfig) pkgconfig(libpcre) pkgconfig(libpng) pkgconfig(libwebp) pkgconfig(libxslt)
BuildRequires:  pkgconfig(QtCore) pkgconfig(QtNetwork) pkgconfig(sqlite3) pkgconfig(xext) pkgconfig(xrender)
BuildRequires:  perl(version) perl(Digest::MD5) perl(Getopt::Long) python2 ruby ruby(rubygems)
BuildRequires:  pkgconfig(gstreamer-1.0) pkgconfig(gstreamer-app-1.0) pkgconfig(QtLocation) pkgconfig(QtSensors)
Requires:       mozilla-filesystem glib2

%description
WebKit is an open source web browser engine. WebKit's HTML and JavaScript
code began as a branch of the KHTML and KJS libraries from KDE. As part
of KDE framework KHTML was based on Qt but during their porting efforts
Apple's engineers made WebKit toolkit independent. QtWebKit is a project
aiming at porting this fabulous engine back to Qt.

%package        devel
Summary:        Development files for qtwebkit
Requires:       %{name}%{?_isa} = %{version}-%{release}
Obsoletes:      qt-webkit-devel < 1:4.9.0
Provides:       qt-webkit-devel = 2:%{version}-%{release}
Provides:       qt4-webkit-devel = 2:%{version}-%{release}
Provides:       qt4-webkit-devel%{?_isa} = 2:%{version}-%{release}

%description    devel
This package contains libraries and headier files for developing applications
that use qtwebkit.

%prep
%setup -q -c -n webkit-qtwebkit-23
%autopatch -p1
install -m755 -D %{SOURCE1} bin/qmake

%build
CFLAGS="%{optflags} -Wno-expansion-to-defined"; export CFLAGS
CXXFLAGS="%{optflags} -Wno-expansion-to-defined"; export CXXFLAGS
LDFLAGS="%{?__global_ldflags} -Wno-expansion-to-defined"; export LDFLAGS
PATH=`pwd`/bin:%{_qt4_bindir}:$PATH; export PATH
QMAKEPATH=`pwd`/Tools/qmake; export QMAKEPATH
QTDIR=%{_qt4_prefix}; export QTDIR

%ifarch aarch64 %{mips}
%global qtdefines  DEFINES+=ENABLE_JIT=0 DEFINES+=ENABLE_YARR_JIT=0 DEFINES+=ENABLE_ASSEMBLER=0
%endif

mkdir -p %{_target_platform}
pushd    %{_target_platform}
WEBKITOUTPUTDIR=`pwd`; export WEBKITOUTPUTDIR
../Tools/Scripts/build-webkit \
  --qt %{?qtdefines} --no-webkit2 --release \
  --qmakearg="CONFIG+=production_build DEFINES+=HAVE_LIBWEBP=1" \
  --makeargs="%{?_smp_mflags}" --system-malloc
popd

%ifarch %{ix86}
mkdir -p %{_target_platform}-no_sse2
pushd    %{_target_platform}-no_sse2
WEBKITOUTPUTDIR=`pwd`; export WEBKITOUTPUTDIR
../Tools/Scripts/build-webkit \
  --qt --no-webkit2 --release \
  --qmakearg="CONFIG+=production_build DEFINES+=HAVE_LIBWEBP=1" \
  --makeargs="%{?_smp_mflags}" \
  --system-malloc --no-force-sse2
popd
%endif

%install
make install INSTALL_ROOT=%{buildroot} -C %{_target_platform}/Release

%ifarch %{ix86}
mkdir -p %{buildroot}%{_qt4_libdir}/sse2/
mv %{buildroot}%{_qt4_libdir}/libQtWebKit.so.4* %{buildroot}%{_qt4_libdir}/sse2/
make install INSTALL_ROOT=%{buildroot} -C %{_target_platform}-no_sse2/Release
%endif

pushd %{buildroot}%{_libdir}/pkgconfig
grep -v "^Libs.private:" QtWebKit.pc > QtWebKit.pc.new && \
mv QtWebKit.pc.new QtWebKit.pc
popd

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_qt4_libdir}/libQtWebKit.so.4*
%ifarch %{ix86}
%{_qt4_libdir}/sse2/libQtWebKit.so.4*
%endif
%{_qt4_importdir}/QtWebKit/

%files devel
%defattr(-,root,root)
%{_qt4_datadir}/mkspecs/modules/qt_webkit.pri
%{_qt4_headerdir}/*
%{_qt4_libdir}/libQtWebKit.prl
%{_qt4_libdir}/libQtWebKit.so
%{_libdir}/pkgconfig/QtWebKit.pc

%changelog
* Fri Feb 14 2020 Ling Yang <lingyang2@huawei.com> - 2.3.4-29
- Package init
